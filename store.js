var { feach } = require('./fsHandler');
var fs = require('fs');
var { addElement, getElement, removeElement, listAllElements } = require('./controller');
var command = process.argv[2];
var key = process.argv[3];
var value = process.argv[4];
let store = [];
feach('dictionary.json').then(data => {
    store = data;
    switch (command.toLowerCase()) {
        case 'add':
            addElement({
                key: key,
                value: value
            }, store).then(res => {
                store = res;
            })
            break;
        case 'list':
            console.log(`Data Count: ${store.length}`);
            listAllElements(store);
            break;
        case 'get':
            let val = getElement(key, store);
            val ? console.log(val) : console.log(`Not Found`);
            break;
        case 'remove':
            removeElement(key, store).then(res => {
                store = res;
            })
            break;
        case 'clear':
            fs.writeFile("dictionary.json", JSON.stringify([]), (err) => {
                if (!err) {
                   return console.log("Done");
                }
                console.log(err);
            });
            break;
        default:
            console.log("Undefined Command");
            break;
    }
}, error => {

})

