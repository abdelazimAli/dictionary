const fs = require('fs');

function feach(path) {
    return new Promise((resolve,reject)=>{
        fs.readFile(path,(error,data)=>{    
            if(error){
                Promise.reject();
            } 
            resolve([].concat(JSON.parse(data)));    
        });
    })
    // var dic = [];
    // try {
    //     dic = JSON.parse(file)
    // } catch (error) {
    //     dic = [];
    // }
    // return [].concat(dic);
}
function save(path, data) {
    return new Promise((resolve,reject)=>{
        fs.writeFile(path, JSON.stringify(data),(error,response)=>{
            if(error) Promise.reject();
            resolve(data);
        })
    })
        
}
module.exports = {
    feach,
    save
}