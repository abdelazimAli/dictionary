var { save } = require('./fsHandler');
async function addElement(element, store) {
    if (store.constructor === Array) {
        store.push(element);
        try {
            const arr = await save('dictionary.json', store);
            console.log("Done");

            return arr;
        } catch (error) {
            console.log("Save Error");
            return store
        }

    }

}

function listAllElements(store) {
    if (store.constructor === Array) {
        store.forEach((element) => {
            console.log(`${element.key}: ${element.value}`);
        });
    } else {
        console.log("feach Data incorrect");
    }

}

function getElement(key, store) {

    var obj = store.find(ele => ele.key === key);
    return obj ? obj.value : null;
}

async function removeElement(key, store) {
    var dic = store.filter(ele => ele.key !== key);
    if (dic.length !== store.length) {
        try {
            const arr = await save('dictionary.json', dic)
            console.log("Done");
            return arr;
        } catch (error) {
            console.log("Remove Error");
            return store
        }
    } else {
        return console.log("Not Found");

    }


}



module.exports = {
    addElement: addElement,
    getElement: getElement,
    listAllElements: listAllElements,
    removeElement: removeElement
}